# The\Loop

Provides access to an event loop. Backed by Amphp event loop and therefore compatible with the Amphp
libraries.

## Usage

To access the event loop, simply do this:

```php
\The\Loop::run(function() {});
```

### Composer Provides

The framework itself can provide the The\Loop by declaring it in the composer.json file:

```json
{
    "provide": [ 'the/loop' ]
}
```

and then aliasing your class into The\Loop

```php
namespace The {

    class_alias( \Amphp\Loop::class, Loop::class );

}
```


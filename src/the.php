<?php
namespace The;

/**
 * There are no PSR database interfaces. The most standardised interface is PDO which is provided by PHP
 * already.
 *
 * This will provide a Promise based simple database wrapper. It should work both in non-blocking and blocking
 * environments.
 *
 * @return The\Database\DatabaseInterface
 */

/**
 * Must alias the Loop class to The\Loop
 */
\The::setDefaultClass( Loop::class, \Amp\Loop::class );
